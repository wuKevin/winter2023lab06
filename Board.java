public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.tiles = new boolean[12];
		this.die1 = new Die();
		this.die2 = new Die();
	}
	
	public String toString(){
		String display = "";
		for(int i=0; i<tiles.length; i++){
			if(tiles[i]==false){
				display = display+(i+1)+" ";
			}
			else{
				display = display+"X ";
			}
		}
		return display;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println("Die 1: "+die1.getFaceValue()+" Die 2: "+die2.getFaceValue());
		
		int sumOfDice = (die1.getFaceValue())+(die2.getFaceValue());
		
		if(tiles[sumOfDice-1]==false){
			System.out.println("Closing tile equal to sum: "+sumOfDice);
			tiles[sumOfDice-1]=true;
			return false;
		}
		else if(tiles[die1.getFaceValue()-1]==false){
			System.out.println("Closing tile with the same value as die one: "+die1.getFaceValue());
			tiles[die1.getFaceValue()-1]=true;
			return false;
		}
		else if(tiles[die2.getFaceValue()-1]==false){
			System.out.println("Closing tile with the same value as die two: "+die2.getFaceValue());
			tiles[die2.getFaceValue()-1]=true;
			return false;
		}
		else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
}
public class Jackpot{
	public static void main(String[]args){
		
		System.out.println("Welcome to Jackpot!! Presented by Kevin Wu");
		
		Board theBoard = new Board();
		boolean gameOver = false;
		int numberOfTilesClosed = 0;
		
		while(gameOver == false){
			System.out.println(theBoard);
			if(theBoard.playATurn()==true){
				gameOver = true;
			}
			else{
				numberOfTilesClosed++;
			}
		}
		
		if(numberOfTilesClosed >= 7){
			System.out.println("Winner Winner Chicken Dinner! You won Jackpot!");
		}
		else{
			System.out.println("You snooze, you lose... the Jackpot.");
		}
		
	}
}
import java.util.Random;
public class Die{
	private int faceValue;
	private Random rand;
	
	public Die(){
		this.faceValue = 1;
		this.rand = new Random();
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public int roll(){
		this.faceValue = rand.nextInt(6)+1;
		return this.faceValue;
	}
	
	public String toString(){
		return "Face value: " + this.faceValue;
	}
	
}